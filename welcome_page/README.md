# merzlyakova-django-2k-11-200



## Welcome-page of Kazan for navigation
## Запуск проекта для разработки
- 'python3 -m venv venv' - создание виртуального окружения
- 'source venv/bin/activate' - войти в вируальное окружение
- 'pip install -r requirements.txt' - установка зависимостей
- 'python manage.py makemigrations' - создание миграций
- 'python manage.py migrate' - применение миграций
- 'python manage.py runserver' - запустить сервер для разработки
- 'docker-compose up -d' - запустить доп. сервисы в Docker

