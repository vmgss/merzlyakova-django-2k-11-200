from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm

from web.models import Review, Photo

User = get_user_model()


class SearchForm(forms.Form):
    search_query = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Поиск'}))


class RegistrationForm(forms.ModelForm):
    email = forms.EmailField(label='Почта')
    username = forms.CharField(label='Имя пользователя')
    password = forms.CharField(widget=forms.PasswordInput, label='Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Подтверждение пароля')

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password'] != cleaned_data['password2']:
            self.add_error('password', 'Пароли не совпадают')
        return cleaned_data

    class Meta:
        model = User
        fields = ('email', 'username', 'password', 'password2')


class AuthForm(forms.Form):
    username = forms.CharField(label='Имя пользователя')
    password = forms.CharField(widget=forms.PasswordInput, label='Пароль')

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data


class CustomUserChangeForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username']


class CustomPasswordChangeForm(PasswordChangeForm):
    class Meta:
        model = get_user_model()


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['text', 'user', 'place']


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['image', 'place', 'review']
