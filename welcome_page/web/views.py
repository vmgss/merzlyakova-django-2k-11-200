from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login, update_session_auth_hash

from .forms import SearchForm, RegistrationForm, AuthForm, CustomUserChangeForm, ReviewForm, PhotoForm
from .models import Review

User = get_user_model()


def main(request):
    return render(request, 'web/main_page.html', {'main': main})


def search(request):
    if 'q' in request.GET:
        query = request.GET['q']
    else:
        query = ''
    form = SearchForm(initial={'search_query': query})
    context = {'form': form, 'query': query}
    return render(request, 'search_results.html', context)


def registration(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email']
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
    return render(request, 'web/registration.html', {'form': form, 'is_success': is_success})


def auth(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, 'Данные введены неверно')
            else:
                login(request, user)
                return redirect('main')
    return render(request, 'web/auth.html', {'form': form})


@login_required
def profile(request):
    user = request.user
    user_form = CustomUserChangeForm(instance=user)
    password_change_form = PasswordChangeForm(user)

    if request.method == 'POST':
        if 'user_form_submit' in request.POST:
            user_form = CustomUserChangeForm(request.POST, instance=user)
            if user_form.is_valid():
                user_form.save()
                messages.success(request, 'Изменения сохранены.')

        if 'password_change_submit' in request.POST:
            password_change_form = PasswordChangeForm(user, request.POST)
            if password_change_form.is_valid():
                password_change_form.save()
                messages.success(request, 'Пароль изменен успешно.')

    user_reviews = user.review_set.all()

    context = {'user_form': user_form, 'password_change_form': password_change_form, 'user_reviews': user_reviews}
    return render(request, 'web/profile.html', context)


def history_page(request):
    return render(request, 'web/history.html', {'history_page': history_page})


def kuda_page(request):
    return render(request, 'web/kuda.html', {'kuda_page': kuda_page})


def activity(request):
    return render(request, 'web/activity.html', {'activity': activity})


def add_review(request):
    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        photo_form = PhotoForm(request.POST, request.FILES)

        if review_form.is_valid() and photo_form.is_valid():
            review = review_form.save(commit=False)
            review.user = request.user
            review.save()

            photo = photo_form.save(commit=False)
            photo.review = review
            photo.save()

            return redirect('reviews')
    else:
        review_form = ReviewForm()
        photo_form = PhotoForm()

    return render(request, 'add_review.html', {'review_form': review_form, 'photo_form': photo_form})


def reviews(request):
    reviews = Review.objects.all()
    return render(request, 'reviews.html', {'reviews': reviews})
