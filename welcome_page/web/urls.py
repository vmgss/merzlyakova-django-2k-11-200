"""
URL configuration for welcome_page project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from web.views import\
    main, registration, auth,\
    profile, history_page, kuda_page,activity,add_review, reviews
from .views import search

urlpatterns = [
                path('', main, name='main'),
                path('search/', search, name='search'),
                path('registration/', registration, name='registration'),
                path('auth/', auth, name='auth'),
                path('profile/', profile, name='profile'),
                path('history/', history_page, name='history_page'),
                path('kuda_skhodit/', kuda_page, name='kuda_page'),
                path('activity/', activity, name='activity'),
                path('review/add/', add_review, name='review_add'),
                path('reviews/', reviews, name='reviews'),

]
