from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class UserProfile(models.Model):
    username = models.CharField(max_length=50)
    email = models.EmailField()
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.username


class Place(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    category = models.CharField(max_length=20)
    address = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return self.name


class Review(models.Model):
    text = models.TextField(max_length=1500)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)


class Photo(models.Model):
    image = models.ImageField(upload_to='images/')
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
